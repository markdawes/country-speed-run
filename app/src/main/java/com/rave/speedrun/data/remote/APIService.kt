package com.rave.speedrun.data.remote

import com.rave.speedrun.data.local.Country

interface APIService {

    suspend fun getCountries(): CountryResult<List<Country>>
}