package com.rave.speedrun.data.remote

import com.rave.speedrun.data.local.Country
import com.rave.speedrun.data.local.Currency
import com.rave.speedrun.data.local.Language
import com.rave.speedrun.data.remote.dtos.CountryDTO

class CountryRepo(private val api: CountryService) : APIService {

    // Override the getCountries function
    override suspend fun getCountries(): CountryResult<List<Country>> {

        // Use a try/catch block for potential exceptions
        return try {

            // Check the result from the API
            when (val response = api.getCountries()) {

                // If the API call is successful
                is CountryResult.Success -> {

                    // Map the DTO to Country model
                    val countries = response.data.map { countryDTO -> countryDTO.toCountry() }

                    // Return the mapped countries
                    CountryResult.Success(countries)
                }

                // If the API call failed
                is CountryResult.Failure -> {

                    // Return the failure with the exception
                    CountryResult.Failure(response.exception)
                }

                // If the API call is loading
                CountryResult.Loading -> {

                    // Return loading
                    CountryResult.Loading
                }
            }
        }
        catch (e: Exception) {

            // If there is an exception, return failure with exception
            CountryResult.Failure(e)
        }
    }

    // Extension function to convert CountryDTO to Country
    private fun CountryDTO.toCountry(): Country {
        return Country(
            capital = capital,
            code = code,
            currency = Currency(
                code = currency.code,
                name = currency.name,
                symbol = currency.symbol
            ),
            flag = flag,
            language = Language(
                code = language.code,
                name = language.name
            ),
            name = name,
            region = region
        )
    }
}
