package com.rave.speedrun.data.remote

import android.util.Log
import com.rave.speedrun.data.remote.dtos.CountryDTO
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import io.ktor.client.statement.HttpResponse
import io.ktor.client.statement.bodyAsText
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json

class CountryService(private val client: HttpClient) {

    // Suspension function to get countries
    suspend fun getCountries(): CountryResult<List<CountryDTO>> {
        return try {
            val httpResponse = client.get(BASE_URL)

            val result = when (httpResponse.status.value) {
                200 -> handleSuccessResponse(httpResponse)
                else -> handleErrorResponse(httpResponse)
            }

            client.close()
            return result
        } catch (e: Exception) {
            client.close()
            Log.d("Something went wrong:", "${e.localizedMessage}")
            CountryResult.Failure(e)
        }
    }

    // Handle the successful response
    private suspend fun handleSuccessResponse(httpResponse: HttpResponse): CountryResult<List<CountryDTO>> {
        val stringBody = httpResponse.bodyAsText()
        val json = Json { ignoreUnknownKeys = true }
        val response = json.decodeFromString<List<CountryDTO>>(stringBody)

        return CountryResult.Success(response)
    }

    // Handle the error response
    private fun handleErrorResponse(httpResponse: HttpResponse): CountryResult<List<CountryDTO>> {
        return CountryResult.Failure(Exception("Request failed with status code: ${httpResponse.status.value}"))
    }

    companion object {
        private val BASE_URL = "https://gist.githubusercontent.com/peymano-wmt/32dcb892b06648910ddd40406e37fdab/raw/db25946fd77c5873b0303b858e861ce724e0dcd0/countries.json"
    }
}