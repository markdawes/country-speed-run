package com.rave.speedrun.data.remote.dtos

import com.rave.speedrun.data.local.Country
import com.rave.speedrun.data.local.Currency
import com.rave.speedrun.data.local.Language
import kotlinx.serialization.Serializable

@Serializable
data class CountryDTO(
    val capital: String = "",
    val code: String = "",
    val currency: CurrencyDTO = CurrencyDTO(),
    val flag: String = "",
    val language: LanguageDTO = LanguageDTO(),
    val name: String = "",
    val region: String = "",
) {
    fun toCountry(): Country {
        return Country(
            capital = capital,
            code = code,
            currency = Currency(
                code = currency.code,
                name = currency.name,
                symbol = currency.symbol
            ),
            flag = flag,
            language = Language(
                code = language.code,
                name = language.name
            ),
            name = name,
            region = region
        )
    }
}

@Serializable
data class CurrencyDTO(
    val code: String = "",
    val name: String = "",
    val symbol: String? = null
)

@Serializable
data class LanguageDTO(
    val code: String? = null,
    val name: String = "",
    val iso639_2: String? = null,
    val nativeName: String? = null
)