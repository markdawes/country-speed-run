package com.rave.speedrun.data.remote

// A class to handle various results from the Country API call
sealed class CountryResult<out R> {

    // Represents a successful result, contains data of type R
    data class Success<out T>(val data: T) : CountryResult<T>()

    // Represents a failure, contains an Exception
    data class Failure(val exception: Exception) : CountryResult<Nothing>()

    // Represents a state when data is being loaded
    object Loading : CountryResult<Nothing>()
}