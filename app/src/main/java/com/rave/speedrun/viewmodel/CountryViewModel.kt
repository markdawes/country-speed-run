package com.rave.speedrun.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rave.speedrun.data.local.Country
import com.rave.speedrun.data.remote.CountryRepo
import com.rave.speedrun.data.remote.CountryResult
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class CountryViewModel(private val repo: CountryRepo) : ViewModel() {

    private val _countriesLiveData = MutableLiveData<CountryResult<List<Country>>>()
    val countriesLiveData: LiveData<CountryResult<List<Country>>> = _countriesLiveData

    fun getCountries() {
        viewModelScope.launch {
            _countriesLiveData.value = CountryResult.Loading
            delay(3000) // simulation of some processing
            val result = repo.getCountries()
            _countriesLiveData.value = result
        }
    }
}