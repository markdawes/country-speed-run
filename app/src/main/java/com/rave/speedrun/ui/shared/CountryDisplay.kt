package com.rave.speedrun.ui.shared

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.Divider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.rave.speedrun.data.local.Country

@Composable
fun CountryDisplay(countries: List<Country>) {
    LazyColumn(contentPadding = PaddingValues(16.dp)) {
        items(countries.size) { index ->
            val country = countries[index]
            Column {
                Row(verticalAlignment = Alignment.CenterVertically) {
                    Text(
                        text = "${country.name}, ${country.region}",
                        modifier = Modifier.weight(1f),
                        style = MaterialTheme.typography.titleLarge
                    )
                    Text(
                        text = country.code,
                        style = MaterialTheme.typography.bodySmall
                    )
                }
                Spacer(modifier = Modifier.height(8.dp))
                Text(
                    text = "Capital: ${country.capital}",
                    style = MaterialTheme.typography.bodySmall
                )
                Spacer(modifier = Modifier.height(16.dp))
                Divider(color = Color.LightGray, thickness = 1.dp) // updated line
                Spacer(modifier = Modifier.height(16.dp))
            }
        }
    }
}