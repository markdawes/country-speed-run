package com.rave.speedrun

import com.rave.speedrun.data.remote.CountryRepo
import com.rave.speedrun.data.remote.CountryResult
import com.rave.speedrun.data.remote.CountryService
import com.rave.speedrun.data.remote.dtos.CountryDTO
import com.rave.speedrun.data.remote.dtos.CurrencyDTO
import com.rave.speedrun.data.remote.dtos.LanguageDTO
import com.rave.speedrun.testUtil.CoroutinesExtension
import com.rave.speedrun.testUtil.InstantTaskExecutorExtension
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(CoroutinesExtension::class, InstantTaskExecutorExtension::class)
class RepoTest {
    // Mock CountryService
    private val countryService: CountryService = mockk()

    // Instantiate CountryRepo with the mock service
    private val countryRepo = CountryRepo(countryService)

    // Define a test case for a successful API response
    @Test
    fun `getCountries returns Success when api getCountries is successful`() = runBlockingTest {
        // Define successful API response
        val countryDto = CountryDTO(capital = "Dummy Capital",
            code = "DUM",
            currency = CurrencyDTO(
                code = "DUC",
                name = "Dummy Currency",
                symbol = "$"
            ),
            flag = "https://dummy.flag.url",
            language = LanguageDTO(
                code = "DUL",
                name = "Dummy Language"
            ),
            name = "Dummy Country",
            region = "Dummy Region")
        val apiResponse = CountryResult.Success(listOf(countryDto))

        // Arrange
        coEvery { countryService.getCountries() } returns apiResponse

        // Act
        val result = countryRepo.getCountries()

        // Assert
        assertTrue(result is CountryResult.Success)
        assertEquals(countryDto.toCountry(), result)
    }

    // Define a test case for an API failure response
    @Test
    fun `getCountries returns Failure when api getCountries fails`() = runTest {
        // Define failure API response
        val apiResponse = CountryResult.Failure(Exception("API call failed"))

        // Arrange
        coEvery { countryService.getCountries() } returns apiResponse

        // Act
        val result = countryRepo.getCountries()

        // Assert
        assertTrue(result is CountryResult.Failure)
        assertEquals("API call failed", result.exception.message)
    }

    // TODO: Add more test cases for different scenarios
}}